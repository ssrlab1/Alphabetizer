<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'Alphabetizer.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = Alphabetizer::loadLanguages();
	Alphabetizer::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo Alphabetizer::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', Alphabetizer::showMessage('default input'))); ?>";
			
			function ajaxRequest() {
				var caseSensitive = $('input:checkbox[name=caseSensitive]').prop('checked') == true ? 1 : 0;
				var removeDuplicates = $('input:checkbox[name=removeDuplicates]').prop('checked') == true ? 1 : 0;
				var reverseDictionary = $('input:checkbox[name=reverseDictionary]').prop('checked') == true ? 1 : 0;
				var alignRight = $('input:checkbox[name=alignRight]').prop('checked') == true ? 1 : 0;
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/Alphabetizer/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'text': $('textarea#inputTextId').val(),
						'ignoreCharacters': $('#ignoreCharactersId').val(),
						'entryDivider': $('#entryDividerId').val(),
						'alphabet': $('input[name=alphabet]:checked').val(),
						'custom': $("input:text[name=Custom]").val(),
						'sorting': $('input[name=sorting]:checked').val(),
						'caseSensitive' : caseSensitive,
						'removeDuplicates' : removeDuplicates,
						'reverseDictionary' : reverseDictionary,
						'alignRight' : alignRight
					},
					success: function(msg){
						$('#resultId').empty();
						
						var result = jQuery.parseJSON(msg);
						output = document.getElementById("resultId");
						
						var statisticsElem = document.createElement("div");
						statisticsElem.innerHTML = "<b><?php echo Alphabetizer::showMessage('statistics'); ?></b>: <b>" + result.statistics + "</b>";
						output.appendChild(statisticsElem);
						
						var resultTextElem = document.createElement("textarea");
						resultTextElem.setAttribute("class", "form-control");
						resultTextElem.setAttribute("rows", "10");
						resultTextElem.setAttribute("readonly", "");
						if(alignRight) {
							resultTextElem.style.textAlign = "right";
						}
						var resultTextNode = document.createTextNode(result.result);
						resultTextElem.appendChild(resultTextNode);
						
						output.appendChild(resultTextElem);
					},
					error: function(){
						$('#resultId').html('ERROR');
					}
				});
			}
			
			$(document).ready(function () {
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/Alphabetizer/api.php',
					data: {
						'localization': '<?php echo $lang; ?>'
					},
					success: function(msg){
						var result = jQuery.parseJSON(msg);
						$('#alphabetsSet').html(result.result);
					},
					error: function(){
						$('#alphabetsSet').html('ERROR');
					}
				});
				$(document).on('click', '#alphabetsHeader', function(){
					$('#alphabetsSet').slideToggle('slow');
				});
				$(document).on('click', 'button#MainButtonId', function(){
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					ajaxRequest();
				});
				$(document).on('keypress', 'input[type=text]', function (e) {
					if (e.which == 13) {
						$('#resultBlockId').show('slow');
						$('#resultId').empty();
						$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
						ajaxRequest();
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/Alphabetizer/?lang=<?php echo $lang; ?>"><?php echo Alphabetizer::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo Alphabetizer::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo Alphabetizer::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = Alphabetizer::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . Alphabetizer::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo Alphabetizer::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo Alphabetizer::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo Alphabetizer::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", Alphabetizer::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group row">
						<label for="ignoreCharactersId" class="col-sm-4 col-form-label"><?php echo Alphabetizer::showMessage('ignore characters'); ?></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="ignoreCharacters" id="ignoreCharactersId" value="<?php echo "'ʼ’‘′-̀́"; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group row">
						<label for="consonantsBelId" class="col-sm-4 col-form-label"><?php echo Alphabetizer::showMessage('entry divider'); ?></label>
						<div class="col-sm-8">
							<select id="entryDividerId" name="entryDivider" class="selector-primary">
								<option value="newline"><?php echo Alphabetizer::showMessage('newline'); ?></option>
								<option value="space"><?php echo Alphabetizer::showMessage('space'); ?></option>
								<option value="comma"><?php echo Alphabetizer::showMessage('comma'); ?></option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-sm-4">
						<input type="radio" name="sorting" value="a-z" checked>
						<?php echo Alphabetizer::showMessage('sorting a-z'); ?>
						<br />
						<input type="radio" name="sorting" value="z-a">
						<?php echo Alphabetizer::showMessage('sorting z-a'); ?>
					</div>
					<div class="col-sm-8">
						<input type="checkbox" name="caseSensitive" value="1" checked>&nbsp;<?php echo Alphabetizer::showMessage('case sensitive'); ?></input>
						<br>
						<input type="checkbox" name="removeDuplicates" value="1">&nbsp;<?php echo Alphabetizer::showMessage('remove duplicates'); ?></input>
						<br>
						<input type="checkbox" name="reverseDictionary" value="1">&nbsp;<?php echo Alphabetizer::showMessage('reverse dictionary'); ?></input>
						<br>
						<input type="checkbox" name="alignRight" value="1">&nbsp;<?php echo Alphabetizer::showMessage('right align'); ?></input>
					</div>
				</div>
				<div class="col-md-12">
					<h4><?php echo Alphabetizer::showMessage('custom sort'); ?></h4>
				</div>
				<div class="col-md-12">
					<div class="col-sm-4">
						<input type="radio" name="alphabet" value="Custom" checked>
						<?php echo Alphabetizer::showMessage('custom alphabet'); ?>
					</div>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="Custom" id="CustomId" value="АаБбВвГгДдЕеЁёЖжЗзІіЙйКкЛлМмНнОоПпРрСсТтУуЎўФфХхЦцЧчШшЫыЬьЭэЮюЯя">
						<p id="alphabetsHeader">
							<a><?php echo Alphabetizer::showMessage('select alphabet'); ?></a>
						</p>
					</div>
				</div>
				<div class="col-md-12" id="alphabetsSet" style="display: none;"></div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo Alphabetizer::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo Alphabetizer::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="statisticsId"></p>
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo Alphabetizer::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/Alphabetizer" target="_blank"><?php echo Alphabetizer::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/Alphabetizer/-/issues/new" target="_blank"><?php echo Alphabetizer::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo Alphabetizer::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo Alphabetizer::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $localizationLang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo Alphabetizer::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php Alphabetizer::sendErrorList($lang); ?>