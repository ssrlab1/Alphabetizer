<?php 
	class Alphabetizer {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $alphabetStr = '';
		private $alphabetArr = array();
		private $ignoreCharacters = '';
		private $entryDivider = '';
		private $caseSensitive = 0;
		private $reverseDictionary = 0;
		private $sortedList = '';
		const BR = "<br>\n";
		
		function __construct($alphabet, $ignoreCharacters, $entryDivider, $sorting, $caseSensitive, $removeDuplicates, $reverseDictionary, $custom = '') {
			$this->caseSensitive = $caseSensitive;
			$this->removeDuplicates = $removeDuplicates;
			$this->reverseDictionary = $reverseDictionary;
			$this->alphabetArr = self::getAlphabets();
			$this->ignoreCharacters = preg_split('//u', $ignoreCharacters, -1, PREG_SPLIT_NO_EMPTY);
			if($entryDivider == 'newline') {
				$this->entryDivider = "\n";
			}
			elseif($entryDivider == 'space') {
				$this->entryDivider = " ";
			}
			elseif($entryDivider == 'comma') {
				$this->entryDivider = ",";
			}
			if($alphabet == 'Custom') {
				$this->alphabetStr = $custom;
			} else {
				$this->alphabetStr = $this->alphabetArr[$alphabet]['alphabet'];
			}
			$alphabetArrTmp = preg_split('//u', $this->alphabetStr, -1, PREG_SPLIT_NO_EMPTY);
			if($sorting == 'z-a') {
				$alphabetArrTmp = array_reverse($alphabetArrTmp);
			}
			foreach($alphabetArrTmp as $k => $letter) {
				if(!$this->caseSensitive) {
					$letter = mb_strtolower($letter, 'UTF-8');
				}
				if(!isset($this->alphabetArr[$letter])) {
					$this->alphabetArr[$letter] = $k;
				}
			}
			//echo '<pre>';
			//var_dump($this->alphabetArr);
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Alphabetizer';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/Alphabetizer/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		static public function getAlphabets() {
			$alphabets = array();
			$filePath = dirname(dirname(__FILE__)) . '/resources/Alphabets.txt';
			$handle = fopen($filePath, 'r') OR die('fail open Alphabets.txt');
			if($handle) {
				while(($buffer = fgets($handle, 4096)) !== false) {
					if(substr($buffer, 0, 1) != '#' && substr($buffer, 0, 2) != '//') {
						$lineArr = preg_split("/\t/", $buffer);
						if(isset($lineArr[0]) && isset($lineArr[1]) && isset($lineArr[2])) {
							$alphabets[$lineArr[0]] = array('name' => $lineArr[1], 'alphabet' => $lineArr[2]);
						}
					}
				}
			}
			fclose($handle);
			return $alphabets;
		}
		
		static public function getAlphabetsTable($localizationLang = 'en') {
			$alphabets = array();
			$filePath = dirname(dirname(__FILE__)) . '/resources/Alphabets.txt';
			$handle = fopen($filePath, 'r') OR die('fail open Alphabets.txt');
			if($handle) {
				while(($buffer = fgets($handle, 4096)) !== false) {
					if(substr($buffer, 0, 1) != '#' && substr($buffer, 0, 2) != '//') {
						$lineArr = preg_split("/\t/", $buffer);
						if(isset($lineArr[0]) && isset($lineArr[1]) && isset($lineArr[2])) {
							$alphabets[$lineArr[0]] = array('name' => $lineArr[1], 'alphabet' => $lineArr[2]);
						}
					}
				}
			}
			fclose($handle);
			$alphabetsTable = '';
			foreach($alphabets as $k => $alphabet) {
				$alphabetsTable .= '<div class="col-sm-4">';
				if($localizationLang == 'en') {
					$alphabetsTable .= '<input type="radio" name="alphabet" value="' . $k . '"> ' . $k . '<br>';
				} else {
					$alphabetsTable .= '<input type="radio" name="alphabet" value="' . $k . '"> ' . $alphabet['name'] . '<br>';
				}
				$alphabetsTable .= '</div><div class="col-sm-8">';
				$alphabetsTable .= '<input type="text" class="form-control" name="' . $k . '" value="' . $alphabet['alphabet'] . '" readonly>';
				$alphabetsTable .= '</div>';
			}
			return $alphabetsTable;
		}
		
		public function setText($text) {
			$this->text = $text;
		}
		
		function userSort($a, $b) {
			if($this->ignoreCharacters) {
				$a = str_replace($this->ignoreCharacters, '', $a);
				$b = str_replace($this->ignoreCharacters, '', $b);
			}
			
			$lengthA = mb_strlen($a, 'UTF-8');
			$lengthB = mb_strlen($b, 'UTF-8');
			
			$status = '';
			for($i = 0; $i < ($lengthA > $lengthB ? $lengthB : $lengthA ); $i++) {
				$aCur = mb_substr($a, $i, 1, 'UTF-8');
				$bCur = mb_substr($b, $i, 1, 'UTF-8');
				
				if(!isset($this->alphabetArr[$aCur])) {
					$this->alphabetArr[$aCur] = $this->ordutf8($aCur) + 1000000;
				}
				if(!isset($this->alphabetArr[$bCur])) {
					$this->alphabetArr[$bCur] = $this->ordutf8($bCur) + 1000000;
				}
				
				if($this->alphabetArr[$aCur] < $this->alphabetArr[$bCur]) {
					$status = -1;
					break;
				}
				elseif($this->alphabetArr[$aCur] > $this->alphabetArr[$bCur]) {
					$status = 1;
					break;
				}
				else {
					$status = 0;
				}
			}
			return $status;
		}
		
		private function ordutf8($letter) {
			$offset = 0;
			$code = ord(substr($letter, $offset, 1));
			if($code >= 128) {
				if($code < 224) $bytesnumber = 2;
				elseif($code < 240) $bytesnumber = 3;
				elseif($code < 248) $bytesnumber = 4;
				$codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
				for ($i = 2; $i <= $bytesnumber; $i++) {
					$offset++;
					$code2 = ord(substr($letter, $offset, 1)) - 128;
					$codetemp = $codetemp*64 + $code2;
				}
				$code = $codetemp;
			}
			$offset += 1;
			if($offset >= strlen($letter)) $offset = -1;
			return $code;
		}
		
		private function mb_strrev($string, $encoding = null) {
			if($encoding === null) {
				$encoding = mb_detect_encoding($string);
			}
			$length = mb_strlen($string, $encoding);
			$reversed = '';
			while($length-- > 0) {
				$reversed .= mb_substr($string, $length, 1, $encoding);
			}
			return $reversed;
		}
		
		public function run() {
			$entryArr = explode($this->entryDivider, $this->text);
			foreach($entryArr as &$entry) {
				$entry = trim($entry);
			}
			if(!$this->caseSensitive) {
				foreach($entryArr as &$entry) {
					$entry = mb_strtolower($entry, 'UTF-8');
				}
			}
			if($this->removeDuplicates) {
				$entryArr = array_unique($entryArr);
			}
			if($this->reverseDictionary) {
				foreach($entryArr as &$entry) {
					$entry = $this->mb_strrev($entry, 'UTF-8');
				}
			}
			usort($entryArr, array('Alphabetizer', 'userSort'));
			if($this->reverseDictionary) {
				foreach($entryArr as &$entry) {
					$entry = $this->mb_strrev($entry, 'UTF-8');
				}
			}
			$this->sortedList = implode("\n", $entryArr);
			$this->sortedListCnt = count($entryArr);
		}
		
		public function saveLogFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'Alphabetizer';
			$sendersName = 'Alphabetizer';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "Alphabetizer from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$text_length = mb_strlen($this->text);
			$pages = round($text_length/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->text);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=Alphabetizer&t=in&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Тэкст ($text_length сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mail_body .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				} else {
					$mail_body .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mail_body .= self::BR;
			
			$mail_body .= 'Карыстальнік абраў наступныя наладкі:' . self::BR;
			$mail_body .= '<blockquote><i>';
			$mail_body .= !empty($this->caseSensitive) ? '– упарадкаванне адчувальнае да рэгістру' . self::BR : '– упарадкаванне не адчувальнае да рэгістру' . self::BR;
			$mail_body .= !empty($this->removeDuplicates) ? '– упарадкаванне з выдаленнем паўтораў' . self::BR : '– упарадкаванне без выдалення паўтораў' . self::BR;
			if(!empty($this->reverseDictionary)) $mail_body .= '– упарадкаванне з улікам адваротнага чытання' . self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_alphabet.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->alphabetStr);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			if(mb_strlen($cache_text)) {
				$mail_body .= '– упарадкаванне паводле наступнага алфавіту: <b>' . $cache_text . '</b>' . self::BR;
			}
			$mail_body .= '</i></blockquote>';
			$mail_body .= self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->sortedList);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=Alphabetizer&t=out&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Упарадкаваны спіс пачынаецца так:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$mail_body .= '<blockquote><i>' . str_replace("\n", '<br>', trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mail_body .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getSortedList() {
			return $this->sortedList;
		}
		
		public function getSortedListCnt() {
			return $this->sortedListCnt;
		}
		
		public function getAlphabetStr() {
			return $this->alphabetStr;
		}
	}
?>