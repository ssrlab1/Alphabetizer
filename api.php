<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	
	include_once 'Alphabetizer.php';
	Alphabetizer::loadLocalization($localization);
	
	$msg = '';
	if(!isset($_POST['text'])) {
		$result['result'] = Alphabetizer::getAlphabetsTable($localization);
		$msg = json_encode($result);
	}
	else {
		$text = isset($_POST['text']) ? $_POST['text'] : '';
		$alphabet = isset($_POST['alphabet']) ? $_POST['alphabet'] : '';
		$custom = isset($_POST['custom']) ? $_POST['custom'] : '';
		$ignoreCharacters = isset($_POST['ignoreCharacters']) ? $_POST['ignoreCharacters'] : '';
		$entryDivider = isset($_POST['entryDivider']) ? $_POST['entryDivider'] : 'newline';
		$sorting = isset($_POST['sorting']) ? $_POST['sorting'] : 'a-z';
		$caseSensitive = isset($_POST['caseSensitive']) ? $_POST['caseSensitive'] : 0;
		$removeDuplicates = isset($_POST['removeDuplicates']) ? $_POST['removeDuplicates'] : 0;
		$reverseDictionary = isset($_POST['reverseDictionary']) ? $_POST['reverseDictionary'] : 0;
		$alignRight = isset($_POST['alignRight']) ? $_POST['alignRight'] : 0;
		if(!empty($text)) {
			$Alphabetizer = new Alphabetizer($alphabet, $ignoreCharacters, $entryDivider, $sorting, $caseSensitive, $removeDuplicates, $reverseDictionary, $custom);
			$Alphabetizer->setText($text);
			$Alphabetizer->run();
			
			$result['text'] = $text;
			$result['result'] = $Alphabetizer->getSortedList();
			$result['statistics'] = $Alphabetizer->getSortedListCnt();
			$msg = json_encode($result);
		}
	}
	echo $msg;
?>